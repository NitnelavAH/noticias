import { Injectable } from '@angular/core';

import { Network } from '@ionic-native/network/ngx';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';



@Injectable({
  providedIn: 'root'
})
export class NetworkService {

  constructor(private network: Network, public loadingController: LoadingController, private router: Router) {}

  revisarConexion(){
        // watch network for a disconnection
    let disconnectSubscription = this.network.onDisconnect().subscribe(() => {
      this.presentLoading();
    });

    }

    async presentLoading() {
      const loading = await this.loadingController.create({
        cssClass: 'alerta',
        message: 'Verifica tu conexión a internet 🌐',
      });
      await loading.present();

      let connectSubscription = this.network.onConnect().subscribe(() => {
        console.log('network connected!');
        // We just got a connection but we need to wait briefly
         // before we determine the connection type. Might need to wait.
        // prior to doing any api requests as well.
        setTimeout(() => {
          if (this.network.type === 'wifi') {
            console.log('we got a wifi connection, woohoo!');
          }
        }, 2000);
        loading.dismiss();
        this.router.navigate(['/tabs']);
      });
    }

}
