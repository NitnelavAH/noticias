import { Injectable } from '@angular/core';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Injectable({
  providedIn: 'root'
})
export class CompartirService {

  constructor(private socialSharing: SocialSharing) { }

  compartir(title, sourcename, file, url){
    this.socialSharing.share(
      title,
      sourcename,
      '',
      url
    );
  }
}
