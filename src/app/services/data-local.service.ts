import { Injectable } from '@angular/core';
import { Storage} from '@ionic/storage';
import { Article } from '../interfaces/interfaces';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class DataLocalService {

  noticias: Article[] = [];

  constructor(private storage: Storage, public toastController: ToastController) {
    this.cargarFavoritos();
  }

  guardarNoticia(noticia: Article){

    const existe = this.noticias.find( noti => noti.title === noticia.title );

    if (!existe){
      this.noticias.unshift( noticia );
      this.storage.set('favoritos', this.noticias);
      this.presentToast('Guardado en Favoritos');
    } else {
      this.presentToast('Esta noticia ya existe en tus favoritos');
    }

  }

  async cargarFavoritos(){
    const favoritos = await this.storage.get('favoritos');

    if ( favoritos ){
      this.noticias = favoritos;
    }
  }

  async presentToast(mensaje: string) {
    const toast = await this.toastController.create({
      message: mensaje,
      duration: 2000,
      color: 'dark'
    });
    toast.present();
  }

  eliminarFavorito(noticiaTitulo: string) {
    this.noticias = this.noticias.filter( noti => noti.title !== noticiaTitulo );
    this.storage.set('favoritos', this.noticias);
    this.presentToast('Eliminado de tus favoritos');
  }
}
