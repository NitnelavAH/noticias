import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { NoticiasComponent } from './noticias/noticias.component';
import { NoticiaComponent } from './noticia/noticia.component';
import { BotoninfoComponent } from './botoninfo/botoninfo.component';




@NgModule({
  declarations: [
    NoticiasComponent,
    NoticiaComponent,
    BotoninfoComponent
  ],
  exports: [
    NoticiasComponent,
    BotoninfoComponent
  ],
  imports: [
    CommonModule,
    IonicModule
  ]
})
export class ComponentsModule { }
