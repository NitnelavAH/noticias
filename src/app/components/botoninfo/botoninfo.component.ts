import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-botoninfo',
  templateUrl: './botoninfo.component.html',
  styleUrls: ['./botoninfo.component.scss'],
})
export class BotoninfoComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {}

  redireccionar(){
    this.router.navigate(['/about']);
  }
}
