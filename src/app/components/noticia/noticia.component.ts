import { Component, OnInit, Input } from '@angular/core';
import { Article } from 'src/app/interfaces/interfaces';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { ActionSheetController } from '@ionic/angular';
import { DataLocalService } from '../../services/data-local.service';
import { CompartirService } from '../../services/compartir.service';
import { NavigationExtras, Router } from '@angular/router';

@Component({
  selector: 'app-noticia',
  templateUrl: './noticia.component.html',
  styleUrls: ['./noticia.component.scss'],
})
export class NoticiaComponent implements OnInit {

  @Input() noticia: Article;
  @Input() indice: number;
  @Input() enFavoritos: boolean;

  constructor(private compartirService: CompartirService,
              private iab: InAppBrowser,
              public actionSheetController: ActionSheetController,
              private dataLocalService: DataLocalService,
              ) { }

  ngOnInit() {}

  abrirNoticia(){
    console.log(this.noticia.url);
    const browser = this.iab.create(this.noticia.url, '_self');
  }

  async lanzarMeunu(){
    let guardarBorradBtn;

    if ( this.enFavoritos ){
      guardarBorradBtn = {
        text: 'Eliminar de Favoritos',
        icon: 'trash-outline',
        cssClass: 'action-dark',
        handler: () => {
          console.log('Favorite clicked');
          this.dataLocalService.eliminarFavorito( this.noticia.title );
        }
      };
    } else {
      guardarBorradBtn = {
        text: 'Agregar a Favoritos',
        icon: 'star-outline',
        cssClass: 'action-dark',
        handler: () => {
          console.log('Favorite clicked');
          this.dataLocalService.guardarNoticia( this.noticia );
        }
      };
    }

    console.log('lanzar menu');
    const actionSheet = await this.actionSheetController.create({
      buttons: [  guardarBorradBtn, {
        text: 'Compartir',
        icon: 'share-social-outline',
        cssClass: 'action-dark',
        handler: () => {
          console.log('Share clicked');
          this.compartirService.compartir(
            this.noticia.title,
            this.noticia.source.name,
            '',
            this.noticia.url
          );
        }
      }, {
        text: 'Cancelar',
        icon: 'close-outline',
        cssClass: 'action-dark',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();

  }
}
