import { Component } from '@angular/core';
import { NetworkService } from '../../services/network.service';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  constructor(private networkService: NetworkService) {
    this.networkService.revisarConexion();
  }

}
